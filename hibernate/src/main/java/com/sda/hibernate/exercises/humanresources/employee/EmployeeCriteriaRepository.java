package com.sda.hibernate.exercises.humanresources.employee;

import com.sda.hibernate.exercises.humanresources.HibernateUtil;
import com.sda.hibernate.exercises.humanresources.department.Department;
import com.sda.hibernate.exercises.humanresources.project.Project;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.List;
import java.util.concurrent.Callable;

// -- 10. ex pag 73 p.2-4

public class EmployeeCriteriaRepository {
   private static final SessionFactory SESSION_FACTORY = HibernateUtil.getInstance();

   public List<Employee> findAll(){
       try(Session session = SESSION_FACTORY.openSession()){
           CriteriaBuilder cb = session.getCriteriaBuilder();
           CriteriaQuery<Employee> cq = cb.createQuery(Employee.class);
           Root<Employee> employee = cq.from(Employee.class);
           CriteriaQuery<Employee> selectAll = cq.select(employee);
           return session.createQuery(selectAll).list();
       }
   }

   public List<Employee> displayAllEmployeesWorkingOn(String departmentName){
       try(Session session = SESSION_FACTORY.openSession()){
       CriteriaBuilder builder =  session.getCriteriaBuilder();
       CriteriaQuery<Employee> cq = builder.createQuery(Employee.class);
       Root<Employee> employeeRoot = cq.from(Employee.class);
       Join<Employee,Department> department = employeeRoot.join("department", JoinType.LEFT);
       cq.multiselect(employeeRoot,department);
       cq.where(builder.equal(department.get("name"),departmentName));
       return session.createQuery(cq.select(employeeRoot)).list();
       }
   }

    public static void main(String[] args) {
        EmployeeCriteriaRepository repo = new EmployeeCriteriaRepository();

        repo.findAll().forEach(System.out::println);
        repo.displayAllEmployeesWorkingOn("Marketing").forEach(System.out::println);
    }
}

package com.sda.hibernate.exercises.humanresources.employee;

import com.sda.hibernate.exercises.humanresources.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.Embedded;
import java.util.Optional;

public class EmployeeRepository {

    private static final SessionFactory SESSION_FACTORY = HibernateUtil.getInstance();
    private static final EmployeeRepository INSTANCE = new EmployeeRepository();

    private EmployeeRepository() {
    }

    public static EmployeeRepository getINSTANCE() {
        return INSTANCE;
    }

    public Optional<Employee> getById(int id){
        try(Session session = SESSION_FACTORY.openSession()){
            Employee employee = session.find(Employee.class,id);
            return employee == null ?
                    Optional.empty(): Optional.of(employee);
        }
    }

    public void changeFirstNameById(int id, String firstName){
        try(Session session = SESSION_FACTORY.openSession()){
            session.beginTransaction();
            Optional<Employee> employeeById = getById(id);
            employeeById.ifPresent(employee -> employee.setFirstName(firstName));
            employeeById.ifPresent(session::update);
            session.getTransaction().commit();
        }
    }

    public void addEmployee(Employee employee){
        try(Session session = SESSION_FACTORY.openSession()){
            session.beginTransaction();
            session.save(employee);
            session.getTransaction().commit();
        }
    }

    public void deleteEmployee(int id){
        try(Session session = SESSION_FACTORY.openSession()){
            session.beginTransaction();
            Optional<Employee> emplyeeById = getById(id);
            emplyeeById.ifPresent(employee -> session.delete(employee));
            session.getTransaction().commit();
        }
    }

}

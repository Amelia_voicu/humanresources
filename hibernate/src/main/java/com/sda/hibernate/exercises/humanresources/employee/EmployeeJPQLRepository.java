package com.sda.hibernate.exercises.humanresources.employee;

import com.sda.hibernate.exercises.humanresources.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.util.List;

// -- 10. ex pag 73 p.2-4

public class EmployeeJPQLRepository {
    private static final SessionFactory SESSION_FACTORY = HibernateUtil.getInstance();

    public List<Employee> displayAllEmployees(){
        try(Session session = SESSION_FACTORY.openSession()){
            Query<Employee> sqlQuery =session.createQuery( "FROM Employee",Employee.class);
            return sqlQuery.list();
        }
    }

    public List<Employee> displayAllEmployeesStartingWithLetterJ(){
        try(Session session = SESSION_FACTORY.openSession()){
            Query<Employee> sqlQuery = session.createQuery("SELECT e FROM Employee e WHERE e.firstName LIKE 'J%'",Employee.class);
            return sqlQuery.list();
        }
    }

    public List<Employee> displayAllEmployeesWorkingOn(String departmentName){
        try(Session session = SESSION_FACTORY.openSession()){
            Query<Employee> sqlQuery = session.createQuery("SELECT e FROM Employee e JOIN e.department d WHERE d.name =:departmentName",Employee.class);
            sqlQuery.setParameter("departmentName",departmentName);
            return sqlQuery.list();
        }
    }

    public List<Employee> displayAllEmployeesAlphabetically(){
        try(Session session = SESSION_FACTORY.openSession()){
            Query<Employee> sqlQuery = session.createQuery("FROM Employee e ORDER BY e.firstName",Employee.class);
            return sqlQuery.list();
        }
    }

    public static void main(String[] args) {

        EmployeeJPQLRepository employeeJPQLRepository = new EmployeeJPQLRepository();
        employeeJPQLRepository.displayAllEmployees().forEach(System.out::println);
        System.out.println("*****************************");
        employeeJPQLRepository.displayAllEmployeesStartingWithLetterJ().forEach(System.out::println);
        System.out.println("*****************************");
        employeeJPQLRepository.displayAllEmployeesWorkingOn("Marketing").forEach(e->System.out.println(e.getFirstName()+" "+e.getLastName()+" "+e.getDepartment().getName()));
        System.out.println("*****************************");
        employeeJPQLRepository.displayAllEmployeesAlphabetically().forEach(e->System.out.println(e.getFirstName() +" "+ e.getLastName()));
    }
}


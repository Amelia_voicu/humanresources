package com.sda.hibernate.exercises.humanresources.project;

import com.sda.hibernate.exercises.humanresources.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;


// -- 10. ex pag 73 p.1
public class ProjectQueriesRepository {

    private static final SessionFactory SESSION_FACTORY = HibernateUtil.getInstance();

    public List<Project> findAllNative(){
        try (Session session = SESSION_FACTORY.openSession()){
            NativeQuery<Project> sqlQuery = session.createSQLQuery("SELECT * FROM projects").addEntity(Project.class);
            List<Project> list = sqlQuery.list();
            return list;
        }
    }

    public List<Project> findAllHQL(){
        try(Session session = SESSION_FACTORY.openSession()){
            Query<Project> sqlQuery = session.createQuery("FROM Project",Project.class);
            return sqlQuery.list();
        }
    }

    public List<Project> findAllCriteria(){
        try(Session session = SESSION_FACTORY.openSession()){
            CriteriaBuilder cb = session.getCriteriaBuilder();
            CriteriaQuery<Project> sqlQuery = cb.createQuery(Project.class);
            Root<Project> project = sqlQuery.from(Project.class);
            CriteriaQuery<Project> all = sqlQuery.select(project);
            TypedQuery<Project> allQuery = session.createQuery(all);
            return allQuery.getResultList();

        }
    }

}

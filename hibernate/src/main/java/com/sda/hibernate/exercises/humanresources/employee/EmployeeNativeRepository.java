package com.sda.hibernate.exercises.humanresources.employee;

import com.sda.hibernate.exercises.humanresources.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;

import java.util.List;

// -- 10. ex pag 73 p.2-4

public class EmployeeNativeRepository {
    private final static SessionFactory SESSION_FACTORY = HibernateUtil.getInstance();

    public List<Employee> displayAll(){
        try(Session session = SESSION_FACTORY.openSession()){
            NativeQuery<Employee> sqlQuery = session.createNativeQuery("SELECT * FROM employees").addEntity(Employee.class);
            return sqlQuery.list();
        }
    }

    public List<Employee> displayAllEmployeesStartingWithLetterJ(){
        try(Session session = SESSION_FACTORY.openSession()){
            NativeQuery<Employee> sqlQuery = session.createNativeQuery("SELECT * FROM employees WHERE first_name LIKE 'J%'").addEntity(Employee.class);
            return sqlQuery.list();
        }
    }

    public List<Employee> displayAllEmployeesWorkingOnFinanceDepartment(String departmentName){
        try(Session session = SESSION_FACTORY.openSession()){
            NativeQuery<Employee> sqlQuery = session.createNativeQuery("SELECT * FROM employees e, departments d WHERE e.department_id = d.department_id AND d.name = :departmentName").addEntity(Employee.class);
            sqlQuery.setParameter("departmentName",departmentName);
            return sqlQuery.list();
        }
    }

    public List<Employee> displayEmployeesAlphabetically(){
        try(Session session = SESSION_FACTORY.openSession()){
            NativeQuery<Employee> sqlQuery = session.createNativeQuery("SELECT * FROM employees ORDER BY first_name").addEntity(Employee.class);
            return sqlQuery.list();
        }
    }

    public static void main(String[] args) {

        EmployeeNativeRepository repo = new EmployeeNativeRepository();
        System.out.println("*****************************");
        repo.displayAll().forEach(System.out::println);
        System.out.println("*****************************");
        repo.displayAllEmployeesStartingWithLetterJ().forEach(System.out::println);
        System.out.println("*****************************");
        repo.displayAllEmployeesWorkingOnFinanceDepartment("Marketing").forEach(e->System.out.println(e.getFirstName()+" "+e.getLastName()+" "+e.getDepartment().getName()));
        System.out.println("*****************************");
        repo.displayEmployeesAlphabetically().forEach(e->System.out.println(e.getFirstName() +" "+ e.getLastName()));
    }
}

package com.sda.hibernate.exercises.humanresources;

import com.sda.hibernate.exercises.humanresources.department.Department;
import com.sda.hibernate.exercises.humanresources.department.DepartmentRepository;
import com.sda.hibernate.exercises.humanresources.employee.Employee;
import com.sda.hibernate.exercises.humanresources.employee.EmployeeRepository;
import com.sda.hibernate.exercises.humanresources.project.Project;
import com.sda.hibernate.exercises.humanresources.project.ProjectQueriesRepository;
import com.sda.hibernate.exercises.humanresources.project.ProjectRepository;

import java.time.LocalDate;

public class Demo {

    public static void main(String[] args) {

        EmployeeRepository employeeRepository = EmployeeRepository.getINSTANCE();
        employeeRepository.getById(1).ifPresent(System.out::println);
        employeeRepository.getById(7).ifPresent(System.out::println);

       /* //adaugam un angajat nou

        Employee employee = new Employee();
        employee.setFirstName("Hannah");
        employee.setLastName("Hazel");
        employee.setDateOfBirth(LocalDate.of(1989,03,27));

        Department department = new Department();
        department.setId(1);

        //setez relatiile
        employee.setDepartment(department);
        department.addEmployee(employee);

        employeeRepository.addEmployee(employee);

        */

       employeeRepository.deleteEmployee(9);
       DepartmentRepository departmentRepository = DepartmentRepository.getINSTANCE();
       departmentRepository.deleteDepartment(6);

        ProjectQueriesRepository repo = new ProjectQueriesRepository();
        repo.findAllCriteria().forEach(System.out::println);
        System.out.println("*****************************");
        repo.findAllHQL().forEach(System.out::println);
        System.out.println("*****************************");
        repo.findAllNative().forEach(System.out::println);


    }
}

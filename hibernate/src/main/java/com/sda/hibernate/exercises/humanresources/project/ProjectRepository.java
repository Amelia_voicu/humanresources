package com.sda.hibernate.exercises.humanresources.project;

import com.sda.hibernate.exercises.humanresources.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.Optional;

public class ProjectRepository {

    public static final SessionFactory SESSION_FACTORY = HibernateUtil.getInstance();
    public static final ProjectRepository INSTANCE = new ProjectRepository();

    private ProjectRepository() {
    }

    public ProjectRepository getInstance() {
        return INSTANCE;
    }

    public Optional<Project> getByID(int id) {
        try (Session session = SESSION_FACTORY.openSession()) {
            Project projectById = session.find(Project.class, id);
            return projectById == null ? Optional.empty() : Optional.of(projectById);
        }
    }

    public void addProject(Project project) {
        try (Session session = SESSION_FACTORY.openSession()) {
            session.beginTransaction();
            session.save(project);
            session.getTransaction().commit();
        }
    }

    public void deleteProject(int id) {
        try (Session session = SESSION_FACTORY.openSession()) {
            session.beginTransaction();
            Optional<Project> projectById = getByID(id);
            projectById.ifPresent(project -> session.delete(project));
            session.getTransaction().commit();
        }
    }
}


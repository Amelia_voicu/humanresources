package com.sda.hibernate.exercises.humanresources.department;

import com.sda.hibernate.exercises.humanresources.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.Optional;

public class DepartmentRepository {
    private static final DepartmentRepository INSTANCE = new DepartmentRepository();
    private static final SessionFactory SESSION_FACTORY =  HibernateUtil.getInstance();

    private DepartmentRepository(){}

    public static DepartmentRepository getINSTANCE() {
        return INSTANCE;
    }

    private Optional<Department> findById(int id){
        try(Session session = SESSION_FACTORY.openSession()){
            Department department = session.find(Department.class,id);
            return department == null ? Optional.empty() : Optional.of(department);
        }
    }

    public void addDepartment(Department d){
        try(Session session = SESSION_FACTORY.openSession()){
            session.beginTransaction();
            session.save(d);
            session.getTransaction().commit();
        }
    }

    public void deleteDepartment(int id){
        try(Session session = SESSION_FACTORY.openSession()){
            session.beginTransaction();
            Optional<Department> department = findById(id);
            department.ifPresent(session::delete);
            session.getTransaction().commit();
        }
    }
}
